<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 1997-2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="pvm_barrier">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>pvm_barrier</refname>
    <refpurpose> blocks the calling process until all processes in a group have called it.  </refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[info] = pvm_barrier(group,count)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>type</term>
        <listitem>
          <para>string,  name of an existing group.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>count</term>
        <listitem>
          <para>integer, specifying the number of group members that must call pvm_barrier before they are all released.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>info</term>
        <listitem>
          <para>integer, status code returned by the routine.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><literal>pvm_barrier</literal> blocks the calling process until <literal>count</literal>
    members of the <literal>group</literal> have called <literal>pvm_barrier</literal>.
    The <literal>count</literal> argument is required because processes could be
    joining the given group after other processes have called
    <literal>pvm_barrier</literal>. Thus PVM does not know how many group members
    to wait for at any given instant. Although <literal>count</literal> can be set less,
    it is typically the total number of members of the group.
    So the logical function of the <literal>pvm_barrier</literal> call
    is to provide a group synchronization.
    During any given barrier call all participating group members
    must call barrier with the same count value.
    Once a given barrier has been successfully passed,
    <literal>pvm_barrier</literal> can be called again by the same group using the same
    group name.</para>
    <para>
    The returned value <literal>info</literal>will be zero if 
    <literal>pvm_barrier</literal> is successful and will be 
    will be &lt; 0 if some error occurs.</para>
  </refsection>
  <refsection role="see also">
<title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="pvm_joingroup">pvm_joingroup</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
