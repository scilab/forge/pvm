// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gw_c()

  src_c_path = fullpath(get_absolute_file_path("builder_gateway_c.sce") + "../../src/c");
  CFLAGS = ilib_include_flag(src_c_path);

  // PutLhsVar managed by user in sci_sum and in sci_sub
  // if you do not this variable, PutLhsVar is added
  // in gateway generated (default mode in scilab 4.x and 5.x)
  WITHOUT_AUTO_PUTLHSVAR = %t;

  functions_list = ["pvm_joingroup","sci_pvm_joingroup";
  "pvm_lvgroup","sci_pvm_lvgroup";
  "pvm_gsize","sci_pvm_gsize";
  "pvm_gettid","sci_pvm_gettid";
  "pvm_getinst","sci_pvm_getinst";
  "pvm_barrier","sci_pvm_barrier";
  "pvm_bcast","sci_pvm_bcast";
  "pvm_tasks","sci_pvm_tasks";
  "pvm_config","sci_pvm_config";
  "pvm_addhosts","sci_pvm_addhosts";
  "pvm_delhosts","sci_pvm_delhosts";
  "pvm_parent","sci_pvm_parent";
  "pvm_tidtohost","sci_pvm_tidtohost";
  "pvm_set_timer","sci_pvm_set_timer";
  "pvm_get_timer","sci_pvm_get_timer";
  "pvm_mytid","sci_pvm_mytid";
  "pvm_exit","sci_pvm_exit";
  "pvm_kill","sci_pvm_kill";
  "pvm_spawn","sci_pvm_spawn";
  "pvm_spawn_independent","sci_pvm_spawn_independent";
  "pvm_recv","sci_pvm_recv";
  "pvm_send","sci_pvm_send";
  "pvm_recv_var","sci_pvm_recv_var";
  "pvm_send_var","sci_pvm_send_var";
  "pvm_reduce","sci_pvm_reduce";
  "pvm_start","sci_pvm_start";
  "pvm_halt","sci_pvm_halt";
  "pvm_error","sci_pvm_error";
  "pvm_sci2f77","sci_pvm_sci2f77";
  "pvm_f772sci","sci_pvm_f772sci";
  "pvm_probe","sci_pvm_probe";
  "pvm_bufinfo","sci_pvm_bufinfo";
  "pvm_error_mode","sci_pvm_error_mode"];  
  
  functions_files = ["sci_pvm_joingroup.c", ..
  "sci_pvm_lvgroup.c", ..
  "sci_pvm_gsize.c", ..
  "sci_pvm_gettid.c", ..
  "sci_pvm_getinst.c", ..
  "sci_pvm_barrier.c", ..
  "sci_pvm_bcast.c", ..
  "sci_pvm_tasks.c", ..
  "sci_pvm_config.c", ..
  "sci_pvm_addhosts.c", ..
  "sci_pvm_delhosts.c", ..
  "sci_pvm_parent.c", ..
  "sci_pvm_tidtohost.c", ..
  "sci_pvm_set_timer.c", ..
  "sci_pvm_get_timer.c", ..
  "sci_pvm_mytid.c", ..
  "sci_pvm_exit.c", ..
  "sci_pvm_kill.c", ..
  "sci_pvm_spawn.c", ..
  "sci_pvm_spawn_independent.c", ..
  "sci_pvm_recv.c", ..
  "sci_pvm_send.c", ..
  "sci_pvm_recv_var.c", ..
  "sci_pvm_send_var.c", ..
  "sci_pvm_reduce.c", ..
  "sci_pvm_start.c", ..
  "sci_pvm_halt.c", ..
  "sci_pvm_error.c", ..
  "sci_pvm_sci2f77.c", ..
  "sci_pvm_f772sci.c", ..
  "sci_pvm_probe.c", ..
  "sci_pvm_bufinfo.c", ..
  "sci_pvm_error_mode.c"];
  
  if getos() == "Windows" then
    functions_files = [functions_files, "DllmainPVM.c"];
  end
  
  
  if getos() == "Windows" then
    if win64() then 
      CFLAGS = CFLAGS + " -I" + fullpath(src_c_path + "/../../thirdparty/win64/include/") + " -I" + fullpath(src_c_path + "/../../thirdparty/win64/src/");
    else
      CFLAGS = CFLAGS + " -I" + fullpath(src_c_path + "/../../thirdparty/win32/include/") + " -I" + fullpath(src_c_path + "/../../thirdparty/win32/src/");
    end
  end  

  tbx_build_gateway("pvm_c", ..
                    functions_list, ..
                    functions_files, ..
                    get_absolute_file_path("builder_gateway_c.sce"), ..
                    ["../../src/c/libcpvm"], ..
                    "", ..
                   CFLAGS);
                   
endfunction

builder_gw_c();
clear builder_gw_c; // remove builder_gw_c on stack
