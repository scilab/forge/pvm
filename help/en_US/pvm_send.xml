<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 1997-2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="pvm_send">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>pvm_send</refname>
    <refpurpose> immediately sends (or multicast) data.  </refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>[info] = pvm_send(tids,buff,msgtag)</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>tids</term>
        <listitem>
          <para>row of integers, contains the task IDs of the tasks to be sent to.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>buff</term>
        <listitem>
          <para>scilab variable.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>msgtag : integer, message tag supplied by the user.  msgtag should be</term>
        <listitem>
          <para>&gt;= 0.  It allows the user's program to distinguish between different kinds of messages .</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>info</term>
        <listitem>
          <para>integer, status code returned by the routine. Values less than zero indicate an error.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><literal>pvm_send</literal> 
    sends (or multicasts) a message 
    to the PVM process identified in the <literal>tids</literal> 
    array. Note that the message is not sent to the caller even if listed
    in the array of tids. <literal>msgtag</literal>
    is used to label the content of the message.</para>
    <para>
    The returned value will be &gt;= 0 if 
    the call is successful and will be 
    will be &lt; 0 if some error occurs.</para>
    <para>
    The <literal>pvm_send</literal> routine is asynchronous.  Computation on the sending
    processor resumes as soon as the message is safely on its way to the
    receiving processor.  This is in contrast to synchronous
    communication, during which computation on the sending processor halts
    until the matching receive is executed by the receiving processor.</para>
    <para>
    If a multicast is performed, <literal>pvm_send</literal>  first determines which other
    pvmds contain the specified tasks.  Then passes the message to these
    pvmds which in turn distribute the message to their local tasks
    without further network traffic.</para>
    <para>
    The PVM model guarantees the following about message order.  If task 1
    sends message A to task 2, then task 1 sends message B to task 2,
    message A will arrive at task 2 before message B.  Moreover, if both
    messages arrive before task 2 does a receive, then a wildcard receive
    will always return message A.</para>
    <para>
    Terminating a PVM task immediately after sending a message or messages
    from it may result in those messages being lost.  To be sure, always
    call pvm_exit() before stopping.</para>
  </refsection>
  <refsection role="see also">
<title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="pvm_recv">pvm_recv</link>
      </member>
      <member>
        <link linkend="pvm_bcast">pvm_bcast</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
