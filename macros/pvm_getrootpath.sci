//
//  Copyright (C) 2010 - 2011 - DIGITEO - Allan CORNET
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//

function p = pvm_getrootpath()
  [fs, path] = libraryinfo("pvmlib");
  p = pathconvert(fullpath(path + "../"), %t, %t);
endfunction
