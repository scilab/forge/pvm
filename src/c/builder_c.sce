// This file is released under the 3-clause BSD license. See COPYING-BSD.

// This macro compiles the files

function builder_c()

  src_c_path = get_absolute_file_path("builder_c.sce");

  CFLAGS = ilib_include_flag(src_c_path);
  
  if getos() == "Windows" then
    if win64() then 
      CFLAGS = CFLAGS + " -I" + fullpath(src_c_path + "../../thirdparty/win64/include/") + " -I" + fullpath(src_c_path + "../../thirdparty/win64/src/");
    else
      CFLAGS = CFLAGS + " -I" + fullpath(src_c_path + "../../thirdparty/win32/include/") + " -I" + fullpath(src_c_path + "../../thirdparty/win32/src/");
    end
  end

  c_files = ["pvm_grp.c", ..
        "pvm_info.c", ..
        "pvm_proc_ctrl.c", ..
        "pvm_recv.c", ..
        "pvm_send.c", ..
        "scipvmf77.c", ..
        "sci_tools.c", ..
        "varpack.c", ..
        "getenvc.c", ..
        "GetenvB.c", ..
        "FileExist.c"];

  if getos() == "Windows" then
    c_files = [c_files, "DllmainPVM.c"];
  end

  tbx_build_src("cpvm", ..
                c_files, ..
                "c", .. 
                src_c_path, ..
                "", ..
                "", ..
                CFLAGS);
                
endfunction

builder_c();
clear builder_c; // remove builder_c on stack
