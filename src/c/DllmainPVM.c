/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2006 - INRIA - Allan CORNET
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <windows.h> 
/*--------------------------------------------------------------------------*/ 
#pragma comment(lib,"Ws2_32.lib")
#pragma comment(lib,"Advapi32.lib")
#pragma comment(lib,"../fortran/libfpvm.lib")
#if _WIN64
  #pragma comment(lib,"../../thirdparty/win64/lib/WIN64/libgpvm3.lib")
  #pragma comment(lib,"../../thirdparty/win64/lib/WIN64/libpvm3.lib")
#else
  #pragma comment(lib,"../../thirdparty/win32/lib/WIN32/libgpvm3.lib")
  #pragma comment(lib,"../../thirdparty/win32/lib/WIN32/libpvm3.lib")
#endif  
/*--------------------------------------------------------------------------*/ 
int WINAPI DllMain (HINSTANCE hInstance , DWORD reason, PVOID pvReserved)
{
  switch (reason) 
    {
    case DLL_PROCESS_ATTACH:
      break;
    case DLL_PROCESS_DETACH:
      break;
    case DLL_THREAD_ATTACH:
      break;
    case DLL_THREAD_DETACH:
      break;
    }
  return 1;
}
/*--------------------------------------------------------------------------*/ 

